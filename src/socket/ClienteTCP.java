package socket;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.net.Socket;

import rsa.Criptografia;
import rsa.GeradorDeChaves;

public class ClienteTCP {

    private static int port = 12345;
    //private static String host = "127.0.0.1";
    private static String host;

    public static void main(String[] args) {
        try {

            host = args[1];

            Socket cliente = new Socket(host, port);
            DataOutputStream saidaDeDados = new DataOutputStream(cliente.getOutputStream());
            DataInputStream entradaDeDados = new DataInputStream(cliente.getInputStream());
            //GeradorDeChaves geradorDeChavesDoCliente = new GeradorDeChaves(29, 37, 1079);
            GeradorDeChaves geradorDeChavesDoCliente = new GeradorDeChaves();
            String mensagemASerEnviadaParaOServidor = "Blz";
            
            System.out.println("O cliente se conectou no servidor");
            
            //Lê a Mensagem enviada pelo servidor (primeira vez) que é a chave pública do servidor
            String mensagemDoServidor = entradaDeDados.readUTF();
            int[] chavePulicaServidor = geradorDeChavesDoCliente.getChaves(mensagemDoServidor);

            System.out.println("A chave pública do servidor é: "
                    + geradorDeChavesDoCliente.getChavePorExtenso(chavePulicaServidor));

            // O cliente envia a chave para o servidor
            saidaDeDados
                    .writeUTF(geradorDeChavesDoCliente.getChavePorExtenso(geradorDeChavesDoCliente.getChavePublica()));
            saidaDeDados.flush();

            // O cliente envia uma mensagem criptografa com a chave pública do servidor para o mesmo.
            saidaDeDados
                    .writeUTF(Criptografia.criptografarMensagem(mensagemASerEnviadaParaOServidor, chavePulicaServidor));
            saidaDeDados.flush();

            // Fica aguardando, até o recebimento da mensagem do servidor
            mensagemDoServidor = entradaDeDados.readUTF();

            // Descriptografa a mensagem (cripotagrafa com a chave pública do cliente) do servidor, com a chave privada
            String mensagemDescriptogradaDoServidor = Criptografia.desCriptografarMensagem(mensagemDoServidor,
                    geradorDeChavesDoCliente.getChavePrivada());

            // Se as mensagem que foi enviada forem iguais, deu tudo certo
            if (mensagemDescriptogradaDoServidor.equals(mensagemASerEnviadaParaOServidor)) {
                System.out.println("Mensagem foi corretamente criptografa e descriptografada com scucesso!");
            }
            cliente.close();
        } catch (Exception e) {
            System.out.println("Erro: " + e.getMessage());
        }
    }

}