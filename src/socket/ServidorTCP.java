package socket;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.net.ServerSocket;
import java.net.Socket;

import rsa.Criptografia;
import rsa.GeradorDeChaves;

public class ServidorTCP {

    static int port = 12345;

    public static void main(String[] args) {
        try {
            ServerSocket servidor = new ServerSocket(port);
            //GeradorDeChaves geradorDeChavesDoServidor = new GeradorDeChaves(17, 11, 7);
            GeradorDeChaves geradorDeChavesDoServidor = new GeradorDeChaves();

            System.out.println("Servidor ouvindo a porta " + port);
            while (true) {

                Socket socket = servidor.accept();
                DataOutputStream saidaDeDados = new DataOutputStream(socket.getOutputStream());
                DataInputStream entradaDeDados = new DataInputStream(socket.getInputStream());
                String host = socket.getInetAddress().getHostAddress();

                System.out.println("Cliente conectado: " + host);

                System.err.println("Enviando chave pública para o cliente: " + host);
                //Primeiramente é enviado a chave pública do servidor para os clientes que forem se conectando
                saidaDeDados.writeUTF(
                        geradorDeChavesDoServidor.getChavePorExtenso(geradorDeChavesDoServidor.getChavePublica()));
                saidaDeDados.flush();
                
                //Recebe a chave enviada pelo cliente
                String chaveDoCliente = entradaDeDados.readUTF();
                
                //Lê a mensagem criptografada (com a chave pública do próprio servidor) enviada pelo cliente
                String mensagemDoCLiente = entradaDeDados.readUTF();
                
                //Transforma a chave (X,Y) em vetor de Inteiros.
                int[] chavePulicaCliente = geradorDeChavesDoServidor.getChaves(chaveDoCliente);
                System.out.println("A chave pública do cliente é: " + geradorDeChavesDoServidor.getChavePorExtenso(chavePulicaCliente));
                
                //Descriptografa a mensagem (cripotagrafada) enviada pelo cliente, com a chave privada do próprio servidor
                String mensagemDescriptogradaDoCliente = Criptografia.desCriptografarMensagem(mensagemDoCLiente, geradorDeChavesDoServidor.getChavePrivada());
                
                //Envia para o cliente a mensagem com a chave pública do cliente
                saidaDeDados.writeUTF(Criptografia.criptografarMensagem(mensagemDescriptogradaDoCliente, chavePulicaCliente));
                saidaDeDados.flush();

            }

        } catch (Exception e)

        {
            System.out.println("Erro: " + e.getMessage());
        } finally

        {

        }
    }

}
