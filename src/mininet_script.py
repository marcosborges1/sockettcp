#!/usr/bin/python

from mininet.topo import Topo
from mininet.net import Mininet
from mininet.util import dumpNodeConnections
from mininet.log import setLogLevel
from datetime import datetime

def log(text):
    filename = open('log/exec.txt', 'a')
    filename.write('%s\n' % text)
    filename.close()

server_prefix = 'primary'
switch_prefix = 'switch'
host_prefix = 'host'
delimiter = '_'
primary_host_ip = '10.1.0.1'
N = 5

class Topologia(Topo):

    def build(self, n=2):
        switch = self.addSwitch('%s%s%s' % (switch_prefix, delimiter, 1))
        primary = self.addHost('%s%s%s' % (server_prefix, delimiter, 1), ip=primary_host_ip)
        
        self.addLink(primary, switch)

        for h in range(n):
            host = self.addHost('%s%s%s' % (host_prefix, delimiter, h+1))
            self.addLink(host, switch)

def main():

    start=datetime.now()


    topo = Topologia(n=N)
    net = Mininet(topo)
    net.start()

    hostname = '%s%s%d' % (server_prefix, delimiter, 1)
    primary = net.get(hostname)

    print primary.cmd("java socket.ServidorTCP %s %s &" % (hostname, primary_host_ip))

    for i in range(N):
        hostname = '%s%s%d' % (host_prefix, delimiter, i+1)
        host = net.get(hostname)
        print '%s: %s' % (hostname, host.IP())
        print host.cmd("java socket.ClienteTCP %s %s" % (hostname, primary_host_ip))

    log('%s: %s' % (N, datetime.now()-start))

    for i in range(N):
        hostname = '%s%s%d' % (host_prefix, delimiter, i+1)
        host = net.get(hostname)
        print host.cmd("sudo killall java")

    print primary.cmd('sudo killall java')
    net.stop()

if __name__ == '__main__':
    setLogLevel('info')
    #setLogLevel('debug')
    #setLogLevel('output')
    main()