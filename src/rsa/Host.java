package rsa;

import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.HashMap;

public class Host {

	private int[] chavePublica;
	private int[] chavePrivada;
	private String name;	
	private ArrayList<String> listaDeMensagensCriptografadasPorHost = new ArrayList<String>();	
	
	public Host(String name) {
		this.name = name;
	}

	public void setChavePublica(int[] chavePublica) {
		this.chavePublica = chavePublica;
	}

	public String getName() {
		return name;
	}

	public void setChavePrivada(int[] chavePrivada) {
		this.chavePrivada = chavePrivada;
	}

	public int[] getChavePublica() {
		return chavePublica;
	}

	public int[] getChavePrivada() {
		return chavePrivada;
	}
	
	public String getChavePorExtenso(int[] chave) {
		return "("+chave[0]+","+chave[1]+")";
	}


	public String toString() {
		return "Host: "+getName()+", chave pública:"+getChavePorExtenso(getChavePublica())+".";
	}

	public void enviarMensagemCriptografada(String mensagemCripografada, Host host) {
		
		host.listaDeMensagensCriptografadasPorHost.add(mensagemCripografada);
		
	}
	public void lerMensagens() throws UnsupportedEncodingException {
		for (String mensagemCripografada: listaDeMensagensCriptografadasPorHost) {
			System.err.println(Criptografia.desCriptografarMensagem(mensagemCripografada, this.getChavePrivada()));
		}
	}
	

//	private void adicionaMensagemNoRepositorio(String mensagemCripografada) {
//		listaDeMensagensCriptografadas.add(mensagemCripografada);
//	}

}
