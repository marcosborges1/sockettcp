# README #

Siga os passos abaixo para a correta execução do socket cliente-servidor em java.

### Clonar o repositório ###

Em um software ou terminal com git, clone o repositório através do seguinte comando:git clone https://marcosborges1@bitbucket.org/marcosborges1/sockettcp.git

### Passos para a execução ###
Para executar os socket, execute nesta ordem as seguintes classes java no pacote socket:

* ServidorTCP.java
* CienteTCP.java

Atenciosamente, Marcos Borges.